package com.stilingue.helloworld;

import java.util.Date;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class HelloWorldController {

	@RequestMapping("/")
	public String index() {
		Date date = new Date();
		return "Hello world. It's: " + date;
	}

}