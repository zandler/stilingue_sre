# Stilingue challenge

### Author: Zandler Oliveira
### Date: 03/09/2020


1. Requirements:
 - GNU/Linux debian-like (Mint itś a good choice)
 - clone this repo inside you PC
 ```bash
    $ git clone https://gitlab.com/zandler/stilingue_sre.git
 ```
 - Change execution permission to deploy.sh
 ```bash
    $ cd stilingue_sre
    $ chmod +x deploy.sh
 ```
  - Execute deploy.sh
  ```bash
    $ sudo ./deploy.sh
 ```
  - Now, go play minecraft, it's take time!
 
 If you cannot deploy, or receive a error, please open a issue.


 Regards,
 Zandler Oliveira