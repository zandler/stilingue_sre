#!/bin/bash

echo "Start config"
# Install kubectl
function deploy_kubectl {
    curl -LO "https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"
    chmod +x kubectl
    sudo mv kubectl /usr/local/bin

    echo "Test kubectl.."
    kubectl version --client
}


# Install helm
function deploy_helm {
    echo "download lastest version from hell (it's a joke)"
    wget https://get.helm.sh/helm-v3.3.0-linux-amd64.tar.gz
    tar -zxvf helm-v3.3.0-linux-amd64.tar.gz
    sudo mv linux-amd64/helm /usr/local/bin/helm
    helm version
}


# Install minikube
function deploy_minikube {
    curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 
    chmod +x minikube
    sudo mv minikube /usr/local/bin

    echo "Minikube version: "
    minikube version client

    echo "start cluster with minikube"
    echo "go to drink a ice tea, smoke or make children's"
    minikube start
}

function config_minikube_host_pv {
    minikube ssh -- sudo mkdir /jenkins
    minikube ssh -- sudo mkdir /gitlab
}

function deploy_gitlab {
    kubectl apply -f gitlab/gitlab_pv.yml
    kubectl apply -f gitlab/gitlab_pvc.yml
    kubectl apply -f gitlab/gitlab_deployment.yml
    kubectl apply -f gitlab/gitlab_servicet.yml
}

function deploy_jenkins {
    kubectl apply -f jenkins/jenkins_pv.yml
    kubectl apply -f jenkins/jenkins_pvc.yml
    kubectl apply -f jenkins/jenkins_deployment.yml
    kubectl apply -f jenkins/jenkins_service.yml
}


deploy_kubectl

deploy_helm

deploy_minikube

config_minikube_host_pv

deploy_gitlab

deploy_jenkins

echo 'Get gitlab url'
minikube service gitlab --url -n ops

echo 'Get Jenkins url'
minikube jenkins gitlab --url -n ops
