#!/bin/bash

# first steps
sudo apt update && sudo apt upgrade -y

# Install requirements
sudo apt install -y git virtualbox zsh 

# Oh-my-zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

# ZSH Plugin
sh -c "$(curl -fsSL https://raw.githubusercontent.com/zdharma/zinit/master/doc/install.sh)"

# Add best plugins
echo "zinit light zdharma/fast-syntax-highlighting"
echo "zinit light zsh-users/zsh-autosuggestions"
echo "zinit light zsh-users/zsh-completions"

# Powerlevel10k 
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k

# Replace theme for powerlevel10k
sed -i '/ZSH_THEME="robbyrussell"/c\ZSH_THEME="powerlevel10k/powerlevel10k"' ~/.ZSHRC

# Load config
source ~/.zshrc

echo "done..."
echo "configure your teminal"
p10k configure







