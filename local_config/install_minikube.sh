#!/bin/bash

# Download minikube
curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 
chmod +x minikube
sudo mv minikube /usr/local/bin

echo "Minikube version: "
minikube version client

echo "start cluster with minikube"
echo "go to drink a ice tea, smoke or make children's"
minikube start
