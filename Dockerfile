FROM maven as build


WORKDIR /app

COPY helloworld/  .
RUN ls
RUN mvn install

FROM openjdk:11-jre

WORKDIR /app
COPY --from=build /app/target/helloworld-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-jar","/app/app.jar"]
